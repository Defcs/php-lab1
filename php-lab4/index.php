<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <main>
        <form action="index.php" method="GET">
            <input type="text" name="stroka">
            <input type="submit">
        </form>
        <?php

            class OperationsRow{
                public $operations= [];
                public $values=[];
                public function add_operation($operation){
                    $this -> operations[] = $operation;
                }
                public function add_value($value){
                    $this -> values[] = $value;
                }
            };
            $string_example= $_GET["stroka"];
            $string_example_strip = str_replace(" ", "", $string_example);

            $current_position = -1;
            $op1=parse_example_string($string_example_strip);


            $answer = calculate($op1);

            echo($string_example." = ".$answer);
            

            function calculate($operation_row){

                calculate_operation_of_row($operation_row, "*");
                calculate_operation_of_row($operation_row, "/");
                calculate_operation_of_row($operation_row, "+");
                calculate_operation_of_row($operation_row, "-");

                return $operation_row->values[0];

            }

            function calculate_operation_of_row($operation_row, $operation_type){


                $result = 0;

                $needed_operations = array_keys($operation_row -> operations, $operation_type);
                $toCount = count($needed_operations);
                
                for ($i = 0; $i < $toCount; $i++){
                    $operation_index = $needed_operations[$i] - $i;
                    $left_operand = $operation_row -> values[$operation_index];
                    $right_operand = $operation_row -> values[$operation_index+1];

                    if ($left_operand instanceof OperationsRow){
                        $left_operand = calculate($left_operand);
                        $operation_row -> values[$operation_index] = $left_operand;
                    }

                    if ($right_operand instanceof OperationsRow){
                        $right_operand = calculate($right_operand);
                        $operation_row -> values[$operation_index+1] = $right_operand;
                    }
                    $current_result = 0;

                    switch ($operation_type) {
                        case "*":
                            $current_result = $left_operand * $right_operand;
                            break;
                        case "/":
                            $current_result = $left_operand / $right_operand;
                            break;
                        case "+":
                            $current_result = $left_operand + $right_operand;
                            break;
                        case "-":
                            $current_result = $left_operand - $right_operand;
                            break;
                        }
                    $result = $result + $current_result;
                    moveOperationsLeft($operation_row, $operation_index);
                    sumValues($operation_row, $operation_index, $current_result);

                };

                return $result;

            }
            
            function moveOperationsLeft($operation_row,$index_from){

                array_splice($operation_row->operations, $index_from, 1);

            }

            function sumValues($operation_row, $index_from, $new_value){

                $operation_row->values[$index_from] = $new_value;
                array_splice($operation_row->values, $index_from + 1, 1);

            }


            function parse_example_string($string_example_strip){
                $operand = "";

                $return_operation_string = new OperationsRow();

                while ($GLOBALS["current_position"] < strlen($string_example_strip)){

                    $GLOBALS["current_position"] ++;
                    $current_position = $GLOBALS["current_position"];

                    switch ($string_example_strip[$current_position]) {
                        case "(":
                            $new_child_row = parse_example_string($string_example_strip);
                            $return_operation_string->add_value($new_child_row);
                            break;
                        case ")":
                            if ($operand != "") {
                                $return_operation_string->values[] = (int) $operand;
                            }
                            $operand = "";
                            return $return_operation_string;
                            break;
                        case ("*"):
                            if ($operand != "") {
                                $return_operation_string->values[] = (int) $operand;
                            }
                            $return_operation_string->operations[] = $string_example_strip[$current_position];
                            $operand = "";
                            break;
                        case ("/"):
                            if ($operand != "") {
                                $return_operation_string->values[] = (int) $operand;
                            }
                            $return_operation_string->operations[] = $string_example_strip[$current_position];
                            $operand = "";
                            break;
                        case ("+"):
                            if ($operand != "") {
                                $return_operation_string->values[] = (int) $operand;
                            }
                            $return_operation_string->operations[] = $string_example_strip[$current_position];
                            $operand = "";
                            break;
                        case ("-"):
                            if ($operand != "") {
                                $return_operation_string->values[] = (int) $operand;
                            }
                            $return_operation_string->operations[] = $string_example_strip[$current_position];
                            $operand = "";
                            break;
                        default:
                            $operand = $operand.$string_example_strip[$current_position];
                            break;
                            
                    }; 
            }

            if ($operand != "") {
                $return_operation_string->values[] = (int) $operand;
                $operand = "";
            }

            return $return_operation_string;
        }
        ?>
    </main>
    <footer>
        Сделать кальькятор Фролов Владислав 211-322
    </footer>
</body>
</html>