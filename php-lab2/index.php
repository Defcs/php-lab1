<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style1.css">
    <title>Document</title>
</head>
<body>
    <header>
        <img src="images/logoPolikek.jpg" alt="logo">
        <p class="description">Лабораторная два.Feedback form.</p>
    </header>
    <main>
        <form action="https://httpbin.org/post" method="post" class="form">
            <label for="1" class="form__label">Ваше имя</label>
            <input type="text" id="1" name="name" class="form__input">
            <label for="2" class="form__label">Ваша фамилия</label>
            <input type="text" id="2" name="surname" class="form__input">
            <label for="3" class="form__label">Тип обращения</label>
            <input name="obr" id="3" list="obr" />
            <datalist id="obr">
                <option value="complain" >
                <option value="predl" >
                <option value="blagodar" >
            </datalist>
            <label for="4" class="form__label">Обращение</label>
            <input type="text" id="4" name="text" class="form__input">
            <label for="5" class="form__label">Получение по смс</label>
            <input type="checkbox" id="5" name="sms" class="form__input">
            <label for="6" class="form__label"> Получение по email</label>
            <input type="checkbox" id="6" name="emale" class="form__input">
            <input type="submit" class="form__submit">
            <a href="post.php" class="form__link">Перейти на Get Header</a>
        </form>
        <?php

        ?>
    </main>
    <footer>
        <p>Собрать сайт из двух страниц. 
1 страница: Сверстать форму обратной связи. Отправку формы осуществить на URL: https://httpbin.org/post. Добавить кнопку для перехода на 2 страницу. 
2 страница: вывести на страницу результат работы функции get_headers. Загрузить код в удаленный репозиторий. Залить на хостинг. 
</p>
    </footer>
</body>
</html>